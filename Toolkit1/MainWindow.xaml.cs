﻿using System;
using System.Windows;
using System.Windows.Controls;
using System.IO;
using System.Diagnostics;
using System.ServiceProcess;

namespace Toolkit1
{
    /// <summary>
    /// Logique d'interaction pour MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        private void button_Click(object sender, RoutedEventArgs e)
        {
            CheckBox[] tabProgtoRun = { Chrome, Adobe, Skype, Zoiper, TeamViewer, Ccleaner };
            String strCmd = "";
            if (!File.Exists(Environment.CurrentDirectory.Substring(0, 3)+"\\setup\\Adobe.exe"))
            {
                try
                {
                    File.Copy(@Environment.CurrentDirectory.Substring(0, 3) + "\\setup\\readerdc.exe", Environment.CurrentDirectory.Substring(0, 3) + "\\setup\\Adobe.exe");
                }
                catch (InvalidCastException) { MessageBox.Show("Erreur dans la copie du fichier" ); }
            }
            foreach (CheckBox c in tabProgtoRun)
            {
                if (c.IsChecked.Value)
                {
                    strCmd = "start /W "+ Environment.CurrentDirectory.Substring(0, 3) + "\\setup\\"+ c.Name.ToString() +".exe";
                    Process process = new Process();
                    process.StartInfo.FileName = "cmd.exe";
                    process.StartInfo.WorkingDirectory = Environment.CurrentDirectory.Substring(0, 2);
                    process.StartInfo.Arguments = "/C"+ strCmd;
                    process.Start();
                }
                
            }
            
        }

        public String getServiceStatus(String value)
        {
            ServiceController sc = new ServiceController(value);

            switch (sc.Status)
            {
                case ServiceControllerStatus.Running:
                    return "Running";
                case ServiceControllerStatus.Stopped:
                    return "Stopped";
                case ServiceControllerStatus.Paused:
                    return "Paused";
                case ServiceControllerStatus.StopPending:
                    return "Stopping";
                case ServiceControllerStatus.StartPending:
                    return "Starting";
                default:
                    return "Status Changing";
            }
        }

        public void ServiceChangeState(String value)
        {
            ServiceController sc = new ServiceController(value);
            //ServiceHelper.ChangeStartMode(sc, ServiceStartMode.Manual);
            sc.Stop();
        }
        private void CbxChrome_Checked(object sender, RoutedEventArgs e)
        {

        }

        private void Grid_Loaded(object sender, RoutedEventArgs e)
        {
            LblServiceStatus.Content = getServiceStatus("Superfetch");
            if (getServiceStatus("Superfetch") != "Running")
            {
                button.IsEnabled = false;
            }
            else
            {
                button.IsEnabled = true;
            }
        }

        private void LblServiceStatus_Loaded(object sender, RoutedEventArgs e)
        {
            
        }

        private void button_Click_1(object sender, RoutedEventArgs e)
        {
            ServiceChangeState("Superfetch");
            LblServiceStatus.Content = getServiceStatus("Superfetch");
            button.IsEnabled = false;
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            while(true)
            {

            }
        }

        private void Toolkit_Loaded(object sender, RoutedEventArgs e)
        {
            if (!File.Exists(Environment.CurrentDirectory.Substring(0, 3) + "\\setup\\Chrome.exe"))
            {
                MessageBox.Show("Toolkit fonctionne avec un dossier appellé \"setup\", assurez vous qu\'il soit dans le même disque que cet application");
            }
        }
    }

}

